#!/bin/bash

for t in 1 2 4 8; do
  echo threads = $t
  GOMAXPROCS=$t perf stat -d -r 100 ./ftcs
done
